const assert = require('assert')
const chai = require('chai')

// require the chai-http module to test HTTP request
const http = require('chai-http')
chai.use(http)

const app = require('../app')

// use the expect assertion style from the chai module
const expect = chai.expect

// make sure our test framework mocha is working as expected
	//describe is used to group unit tests that are related to each other under one test suite

xdescribe('Array', function(){
	describe('indexOf()', function(){
		// it is a single unit test
		it('it should return -1 when the value is not in the array', 
			function(){
			// in a unit test, assertion will be written to tell Mocha what should happen in order for the test to pass
			assert.equal([1,2,3].indexOf(4), -1)
		})
	})
})

//test existence of our application
describe('App', function(){
	it('should exist', function(){
		expect(app).to.be.a('function')
	})

	// use async to indicate that a function will return a promsise object
	it('GET / should return status code 200 and a message', async function(){
		// use the request() method of chai-http to send a GET request to the '/' endpoint in the app module
		await chai.request(app).get('/')
		.then((res)=>{
			// write assertion for the response returned by our async function
			expect(res).to.have.status(200)
			expect(res.body.message).to.contain('It works!')
		})
	})

	// describe a test suite with unit tests covering user registration

	describe('user registration', ()=>{
		it('should return status code 201 and a confirmation for valid input', async ()=>{
			// mock a valid user input
			const new_user = {
				'name': 'John Wick',
				'email': 'baba@yaga.com',
				'password': 'wheresmydog'
			}

			// use chai-htpp to send POST request to the /register endpoint in our app module

			return await chai.request(app).post('/register')
			.type('form')
			//the send() method attaches the mock input to the request
			.send(new_user)
			.then(res=>{
				expect(res).to.have.status(201)
				expect(res.body.message).to.be.equal('User registered.')
				expect(res.body.errors.length).to.be.equal(0)
			})
		})
	})

	describe('user authentication', ()=>{
		it('should return status code 200 and a JWT', async ()=>{
			const valid_input = {
				'email':'baba@yaga.com',
				'password': 'wheresmydog'
			}

			// send a post requet to '/login' endpoint via chai-http
			return await chai.request(app).post('/login')
			.type('form')
			.send(valid_input)
			.then(res=>{
				expect(res).to.have.status(200)
				expect(res.body.token).to.exist
				expect(res.body.message).to.be.equal('Authentication successful.')
				expect(res.body.errors.length).to.be.equal(0)
			})
		})
	})
})

//