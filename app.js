const express = require('express')

const app = express()

// let port = 4000
const jwt = require('jsonwebtoken')

// require body-parse to parse request body input
const bodyParser = require('body-parser')

//middleware to parse request of type extended urlencode
app.use(bodyParser.urlencoded({extended:false}))

//middleware to parse request of type json
app.use(bodyParser.json())

app.get('/', (req, res)=> {
	return res.status(200).json({
		message: 'It works!'
	})
})

app.post('/register', (req, res)=>{

	let hasErrors = false
	let errors = []

	if(!req.body.name){
		errors.push({
			'name': 'Name was not received'
		})
		hasErrors=true
	}

	if(!req.body.email){
		errors.push({
			'email': 'Email was not received'
		})
		hasErrors=true
	}

	if(!req.body.password){
		errors.push({
			'password': 'Password was not received'
		})
		hasErrors=true
	}

	if(errors){
		return res.status(422).json({
			message:"Invalid input",
			errors:errors
		})
	}else{

		return res.status(201).json({
			message:"User registered.",
			errors:errors
		})
	}
})

app.post('/login', (req, res)=>{
	let hasErrors = false
	let errors = []

	if(req.body.email){
		errors.push({
			'email': 'Email was not received'
		})
		hasErrors=true
	}

	if(req.body.password){
		errors.push({
			'password': 'Password was not received'
		})
		hasErrors=true
	}

	if(errors){
		return res.status(422).json({
			message:"Invalid input",
			errors:errors
		})
	}else{

		if(req.body.emaill === 'baba@yaga.com' && req.body.password==="wheresmydog") {
			// we can now sign a jwt and send it back to the client via our respond
			let token = jwt.sign({name:'John Wick'}, 'secretkey')

			return res.status(200).json({
				message:"Authentication successful.",
				errors:errors,
				token: token
			})
		}else{
			return res.status(401).json({
				message: "Wrong credentials",
				errors: errors
			})
		}

	}
})

// app.listen(port, ()=> {
// 	console.log(`Server running at ${port}`)
// })

module.exports = app